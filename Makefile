CC = gcc
CFLAGS = $(OPTFLAGS) -Wall -ggdb3 -D_FILE_OFFSET_BITS=64
OPTFLAGS = -O2
LDFLAGS = -O2 -ggdb3
SQL_LDFLAGS = -L/usr/lib/mysql -L/usr/lib64/mysql -lmysqlclient
TESTLDFLAGS = -ldl -rdynamic -Wl,-export-dynamic
COMMON_OBJS = state.o hash.o cfgfile.o utils.o
DSO_OBJS = $(COMMON_OBJS) module.o hooks.o logging.o sql.o
DSO_LDFLAGS = -shared $(SQL_LDFLAGS) -fPIC -ggdb3
TEST_OBJS = test_utils.o
APP_OBJS = $(COMMON_OBJS) $(TEST_OBJS) lparse.o logutils.o
IMPORT_OBJS = import.o $(APP_OBJS) hooks.o sql.o logging.o
SHOWLOG_OBJS = showlog.o $(APP_OBJS) auth.o
DEPS = Makefile module.h cfgfile.h utils.h
DSO = ndbneb.so
TESTPROG = nebtest
CONFIG_FILE = db.conf
DESTDIR = /tmp/ndbneb
ALL_CFLAGS = $(CPPFLAGS) $(CFLAGS) -fPIC

ifndef V
	QUIET_CC	= @echo '   ' CC $@;
	QUIET_LINK	= @echo '   ' LINK $@;
endif

all: $(DSO) test import showlog

install:	all
	@echo "Installing to $(DESTDIR)"
	@test -d $(DESTDIR) || mkdir -m 755 -p $(DESTDIR)
	@cp -a scripts import showlog $(DSO) $(TESTPROG) $(DESTDIR)
	@cp -a db.conf $(DESTDIR)/ndbneb.conf
	@cp -a *.php $(DESTDIR)
	@chmod 755 $(DESTDIR)/*
	@chmod 644 $(DESTDIR)/ndbneb.conf $(DESTDIR)/scripts/*

test__lparse: test-lparse
	@./test-lparse

test-lparse: test-lparse.o lparse.o logutils.o hash.o test_utils.o
	$(QUIET_LINK)$(CC) $(LDFLAGS) $^ -o $@

$(DSO): $(DSO_OBJS)
	$(QUIET_LINK)$(CC) $(LDFLAGS) $(DSO_LDFLAGS) $^ -o $@

$(TESTPROG): $(TESTPROG).o $(TEST_OBJS)
	$(QUIET_LINK)$(CC) $(LDFLAGS) $(TESTLDFLAGS) $^ -o $@

showlog: $(SHOWLOG_OBJS)
	$(QUIET_LINK)$(CC) $(LDFLAGS) $^ -o $@

import: $(IMPORT_OBJS)
	$(QUIET_LINK)$(CC) $(LDFLAGS) $(SQL_LDFLAGS) $^ -o $@

test: test__lparse test__hash mod $(TESTPROG) $(CONFIG_FILE)
	@./$(TESTPROG) -f $(CONFIG_FILE) $(DSO)

test-sql: test
	@./$(TESTPROG) --test-sql -f $(CONFIG_FILE) $(DSO)

test__hash: test-hash
	@./test-hash

test-hash: test-hash.o hash.o test_utils.o
	$(QUIET_LINK)$(CC) $(LDFLAGS) $(TESTLDFLAGS) $^ -o $@

mod: $(DSO)

%.o:	%.c
	$(QUIET_CC)$(CC) -o $*.o -c $(ALL_CFLAGS) $<

%.so: %.c
	@echo "CC $@"
	@$(CC) $(ALL_CFLAGS) $^ -o $@

clean:
	rm -f core core.* *.{o,so,out,log}
	rm -f $(TESTPROG) import showlog test-lparse test-hash

#'^[^.%][A-Za-z0-9_]*:'
help:
	@echo Available make targets:
	@echo -----------------------
	@$(MAKE) --print-data-base --question | sed -n -e '/^Makefile/d' -e 's/^\([A-Za-z0-9_]*\):.*/\1/p'

## PHONY targets
.PHONY: clean help

# dependencies
$(TESTPROG).o: $(TESTPROG).c $(DEPS)
test-lparse.o: test-lparse.c logutils.h lparse.h Makefile
showlog.o: lparse.h logutils.h showlog.c Makefile
logutils.o: logutils.h Makefile
lparse.o: lparse.c lparse.h Makefile
import.o: lparse.h import.c hooks.h sql.h logging.h Makefile logutils.h
logging.o: logging.c logging.h $(DEPS)
cfgfile.o: cfgfile.c cfgfile.h
sql.o: sql.c sql.h $(DEPS)
hash.o: hash.c hash.h
hooks.o: hooks.c sql.h hooks.h $(DEPS)
module.o: module.c hooks.h $(DEPS)
utils.o: utils.c utils.h
