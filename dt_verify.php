#!/usr/bin/php
<?php

if (empty($argv[1]))
	die("Usage: $argv[0] host1 host2 host3...\n");
$table = getenv('DB_TABLE');
if (!$table)
	$table = 'report_data';

if (!mysql_connect('localhost', 'monitor', 'monitor'))
	die("Failed to connect to mysql: " . mysql_error());
mysql_select_db('monitor_reports');

function dt_verify($host, $table) {
	echo "\n$host:\n";
	$query = "SELECT * FROM $table " .
		"WHERE host_name = '$host' AND " .
		"(event_type = 1103 OR event_type = 1104) ORDER BY id";

	$result = mysql_query($query);

	if (!$result)
		die("mysql_query() failed: " . mysql_error());

	while ($row = mysql_fetch_array($result)) {
		switch ($row['event_type']) {
		 case 1103:
			$start = $row['timestamp'];
			echo "  START: ($start) " . date("Y-m-d H:i:s", $start) . "\n";
			break;
		 case 1104:
			$stop = $row['timestamp'];
			$duration = $stop - $start;
			echo "  STOP: ($stop) " . date("Y-m-d H:i:s", $stop) . " duration=$duration\n";
			break;
		 default:
			echo "laelaelae\n";
		}
	}
}

for ($i = 1; $i < $argc; $i++) {
	dt_verify($argv[$i], $table);
}

?>
