#!/usr/bin/php
<?php
include_once('/opt/monitor/op5/nacoma/include/import.inc.php');

function usage($msg = false)
{
	if ($msg)
		echo $msg . "\n";

	echo "Usage: find_configured.php </path/to/nagios.cfg>\n";
	exit(1);
}

$out = false;
$force = false;
$nagios_cfg = $Config['main'];
if ($argc > 2)
	usage();
if ($argc == 2)
	$nagios_cfg = $argv[1];
if (!is_file($nagios_cfg))
	usage("$nagios_cfg is not a file");

$Config['main'] = $nagios_cfg;
$obj_array = import_object_configuration();
echo join("\n", array_keys($index['host'])) . "\n";
echo join("\n", array_keys($index['service'])) . "\n";
?>
