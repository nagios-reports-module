/*
 * Copyright(C) 2005,2007 op5 AB
 * All rights reserved.
 *
 * See LICENSE.GPL for license details
 *
 * This code is taken from the mrm project and modified to suit
 * the ndbneb database logger
 */

#include "module.h"
#include "sql.h"
#include "hooks.h"
#include "utils.h"
#include "state.h"
#include "nagios/broker.h"
/*
 * sql_query() arguments are usually both numerous and extremely long,
 * and the idiom is common enough that we want to shorten the syntax
 * as much as possible, so do that with this macro
 */
#define RQ return sql_query
#define esc(s) sql_escape(s)

int hook_init(void)
{
	return state_init();
}

int hook_host_result(int cb, void *data)
{
	nebstruct_host_check_data *ds = (nebstruct_host_check_data *)data;

	/* ignore unprocessed and passive checks */
	if (ds->type != NEBTYPE_HOSTCHECK_PROCESSED ||
		cb != NEBCALLBACK_HOST_CHECK_DATA)
	{
		return 0;
	}

	linfo("Check result processed for host '%s'", ds->host_name);

	if (!host_has_new_state(ds->host_name, ds->state, ds->state_type)) {
		linfo("state not changed for host '%s'", ds->host_name);
		return 0;
	}

	RQ("INSERT INTO %s ("
	   "timestamp, event_type, host_name, state, "
	   "hard, retry, output"
	   ") VALUES(%lu, %d, '%s', %d, %d, %d, '%s')",
	   sql_table_name(),
	   ds->timestamp.tv_sec, ds->type, esc(ds->host_name), ds->state,
	   ds->state_type == HARD_STATE, ds->current_attempt,
	   esc(ds->output));
}

int hook_service_result(int cb, void *data)
{
	nebstruct_service_check_data *ds = (nebstruct_service_check_data *)data;

	/* ignore unprocessed and passive checks */
	if (ds->type != NEBTYPE_SERVICECHECK_PROCESSED
		|| cb != NEBCALLBACK_SERVICE_CHECK_DATA)
	{
		return 0;
	}

	linfo("Check result processed for service '%s' on host '%s'",
	      ds->service_description, ds->host_name);

	if (!service_has_new_state(ds->host_name, ds->service_description, ds->state, ds->state_type)) {
		linfo("state not changed for service '%s' on host '%s'",
			  ds->service_description, ds->host_name);
		return 0;
	}

	RQ("INSERT INTO %s ("
	   "timestamp, event_type, host_name, service_description, state, "
	   "hard, retry, output) "
	   "VALUES(%lu, %d, '%s', '%s', '%d', '%d', '%d', '%s')",
	   sql_table_name(),
	   ds->timestamp.tv_sec, ds->type, esc(ds->host_name),
	   esc(ds->service_description), ds->state,
	   ds->state_type == HARD_STATE, ds->current_attempt,
	   esc(ds->output));
}

int hook_downtime(int cb, void *data)
{
	nebstruct_downtime_data *ds = (nebstruct_downtime_data *)data;
	int depth;

	switch (ds->type) {
	case NEBTYPE_DOWNTIME_START:
	case NEBTYPE_DOWNTIME_STOP:
		break;
	case NEBTYPE_DOWNTIME_DELETE:
		/*
		 * if we're deleting a downtime that hasn't started yet, nothing
		 * should be added to the database. Otherwise, transform it to a
		 * NEBTYPE_DOWNTIME_STOP event to mark the downtime as stopped.
		 */
		if (ds->start_time > time(NULL))
			return 0;
		ds->type = NEBTYPE_DOWNTIME_STOP;
		break;
	default:
		return 0;
	}

	if (ds->service_description) {
		depth = dt_depth_svc(ds->type, ds->host_name, ds->service_description);

		linfo("Inserting service downtime entry");
		RQ("INSERT INTO %s"
		   "(timestamp, event_type, host_name,"
		   "service_description, downtime_depth) "
		   "VALUES(%lu, %d, '%s', '%s', %d)",
		   sql_table_name(),
		   ds->timestamp.tv_sec, ds->type, esc(ds->host_name),
		   esc(ds->service_description), depth);
	}

	depth = dt_depth_host(ds->type, ds->host_name);
	linfo("Inserting host downtime_data");
	RQ("INSERT INTO %s"
	   "(timestamp, event_type, host_name, downtime_depth)"
	   "VALUES(%lu, %d, '%s', %d)",
	   sql_table_name(),
	   ds->timestamp.tv_sec, ds->type, esc(ds->host_name), depth);
}

int hook_process_data(int cb, void *data)
{
	nebstruct_process_data *ds = (nebstruct_process_data *)data;

	switch(ds->type) {
	case NEBTYPE_PROCESS_START:
	case NEBTYPE_PROCESS_SHUTDOWN:
		break;
	case NEBTYPE_PROCESS_RESTART:
		ds->type = NEBTYPE_PROCESS_SHUTDOWN;
		break;
	default:
		return 0;
	}

	linfo("Logging Monitor process %s event",
		  ds->type == NEBTYPE_PROCESS_START ? "START" : "STOP");

	RQ("INSERT INTO %s(timestamp, event_type) "
	   "VALUES(%lu, %d)",
	   sql_table_name(), ds->timestamp.tv_sec, ds->type);
}
