#ifndef HOOKS_H
#define HOOKS_H

/* hooks for use as nagios callback handlers */
extern int hook_init(void);
extern int hook_host_result(int cb, void *data);
extern int hook_service_result(int cb, void *data);
extern int hook_downtime(int cb, void *data);
extern int hook_process_data(int cb, void *data);

#endif /* HOOKS_H */
