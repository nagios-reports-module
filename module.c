/*
 * Author: Andreas Ericsson <ae@op5.se>
 *
 * Copyright(C) 2005 op5 AB
 * All rights reserved.
 *
 */

#define NSCORE 1
#include "ndbneb.h"
#include <signal.h>
#include "module.h"
#include "cfgfile.h"
#include "sql.h"
#include "hooks.h"
#include "nagios/nebstructs.h"
#include "nagios/objects.h"
#include "nagios/statusdata.h"

/* Nagios stuff goes below */
NEB_API_VERSION(CURRENT_NEB_API_VERSION);

int cb_handler(int, void *);

static nebmodule *neb_handle;
extern int daemon_dumps_core;
extern int event_broker_options;

static struct callback_struct {
	int type;
	int (*hook)(int, void *);
} callback_table[] = {
	{ NEBCALLBACK_PROCESS_DATA, hook_process_data },
//	{ NEBCALLBACK_LOG_DATA, cb_handler },
//	{ NEBCALLBACK_SYSTEM_COMMAND_DATA, cb_handler },
//	{ NEBCALLBACK_EVENT_HANDLER_DATA, cb_handler },
//	{ NEBCALLBACK_NOTIFICATION_DATA, cb_handler },
	{ NEBCALLBACK_SERVICE_CHECK_DATA, hook_service_result },
	{ NEBCALLBACK_HOST_CHECK_DATA, hook_host_result },
//	{ NEBCALLBACK_COMMENT_DATA, cb_handler },
	{ NEBCALLBACK_DOWNTIME_DATA, hook_downtime },
//	{ NEBCALLBACK_FLAPPING_DATA, cb_handler },
//	{ NEBCALLBACK_PROGRAM_STATUS_DATA, cb_handler },
//	{ NEBCALLBACK_HOST_STATUS_DATA, cb_handler },
//	{ NEBCALLBACK_SERVICE_STATUS_DATA, cb_handler },
};

#define SELF "ndbneb"

static int read_config(const char *arg)
{
	struct cfg_comp *config;
	struct cfg_var *v;
	int ret = 0;
	
	if (!arg || !*arg || !(config = cfg_parse_file(arg))) {
		lerr("Failed to read config from %s", arg ? arg : "(no file)");
		return -1;
	}

	while ((v = next_var(config)))
		ret |= sql_config(v->key, v->value);

	return ret;
}

int nebmodule_init(int flags, char *arg, nebmodule *handle)
{
	char *home = NULL, cwd[PATH_MAX];
	int i;
	nebstruct_process_data proc_start;

	neb_handle = handle;

	linfo("Loading %s", SELF);

	if (read_config(arg) < 0)
		return -1;

	sql_init();
	if (hook_init() < 0)
		return -1;

	proc_start.type = NEBTYPE_PROCESS_START;
	proc_start.timestamp.tv_sec = time(NULL);
	if (hook_process_data(NEBCALLBACK_PROCESS_DATA, &proc_start) < 0)
		return -1;

	/* make sure we can catch whatever we want */
	event_broker_options = BROKER_EVERYTHING;

	ldebug("Forcing coredumps");
	daemon_dumps_core = 1;
	home = getenv("HOME");
	if (!home)
		home = "/tmp";

	signal(SIGSEGV, SIG_DFL);

	/*
	 * if flags == -1, we're running from nebtest, so make sure
	 * coredumps end up in cwd. This *shouldn't* affect anything
	 * (famous last words, no?)
	 */
	if (flags != -1)
		chdir(home);
	getcwd(cwd, sizeof(cwd));
	linfo("Coredumps end up in %s", cwd);

	for (i = 0; i < ARRAY_SIZE(callback_table); i++) {
		struct callback_struct *cb = &callback_table[i];

		neb_register_callback(cb->type, neb_handle, 0, cb->hook);
	}

	return 0;
}

int nebmodule_deinit(int flags, int reason)
{
	int i;
	linfo("Unloading %s", SELF);

	/* flush junk to disk */
	sync();

	log_deinit();

	for (i = 0; i < ARRAY_SIZE(callback_table); i++) {
		struct callback_struct *cb = &callback_table[i];
		neb_deregister_callback(cb->type, cb->hook);
	}

	return sql_close();
}
