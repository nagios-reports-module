#ifndef _NDBNEB_MODULE_H_
#define _NDBNEB_MODULE_H_

#ifndef USE_EVENT_BROKER
# define USE_EVENT_BROKER 1
#endif

/* common include files required for types in this file */
#include "nagios/nebmods.h"
#include "nagios/nebmodules.h"
#include "nagios/nebcallbacks.h"
#include "nagios/nebstructs.h"
#include "nagios/broker.h"

#include "logging.h"

typedef struct file_list {
	char *name;
	struct stat st;
	struct file_list *next;
} file_list;


/* used for Nagios' objects which we build linked lists for */
typedef struct linked_item {
	void *item;
	struct linked_item *next_item;
} linked_item;


/** global variables exported by Nagios **/
extern char *config_file;
extern int event_broker_options;

/** prototypes **/
extern int cb_handler(int cmd, void *data); /* the callback handler */

#endif
