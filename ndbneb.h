/*
 * globally useful cruft goes here
 */

#ifndef _NDBNEB_H_
#define _NDBNEB_H_

#define _GNU_SOURCE 1

#include <stdio.h>

#ifndef __GNUC__
#  define  __attribute__(x)
#endif

#define prefixcmp(a, b) strncmp(a, b, strlen(b))
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#define ISSPACE(c) (c == ' ' || c == '\t')

/*
 * finds the next word in a comma-separated list.
 * if there are multiple commas between each word,
 * it will return a pointer to the second one
 */
static inline char *next_word(char *str)
{
	while (*str && *str != ',' && !ISSPACE(*str))
		str++;

	if (!*str)
		return NULL;

	do {
		while (ISSPACE(*str))
			str++;

		if (*str == ',') {
			str++;
			while (ISSPACE(*str))
				str++;
			break;
		}
	} while (*str++);

	if (*str)
		return str;

	return NULL;
}
#endif
