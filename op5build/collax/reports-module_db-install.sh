#!/bin/bash

# first fix database permissions. We do this unconditionally
mysql -e \
  'GRANT ALL ON monitor_reports.* TO monitor@localhost IDENTIFIED BY "monitor"'
mysql -e 'FLUSH PRIVILEGES'

# next we check if we need to create the database
query="SELECT id FROM report_data LIMIT 1"
mysql monitor_reports -Be "$query" >/dev/null 2>&1 || {
  echo "Creating database"
  mysqladmin create monitor_reports >/dev/null 2>&1 || :
  mysql monitor_reports < /opt/op5/monitor/op5/reports/module/scripts/monitor_reports.sql
}

# since "import" supports incremental imports, we might as well
# always run it. It will never import anything if we're already
# up to date, and it will make retroactively fixing a broken module
# as simple as updating the package
php -d safe_mode=0 /opt/op5/monitor/op5/reports/module/find_configured.php  > /tmp/monitor-reports-module.interesting

# now try to find out if there are any files. This is
# trivial to do in shell, but hard to know if it's a
# user-error or just missing files in import.c
archived="/opt/monitor/var/archives/nagios-*.log"
for f in $archived; do
    if ! [ -f "$f" ]; then
            archived=
    fi
done

nagioslog=/opt/monitor/var/nagios.log
test -f $nagioslog || nagioslog=

# only run the import if there are any files
test "$nagioslog" && \
    /opt/op5/monitor/op5/reports/module/import --incremental \
    --interesting /tmp/monitor-reports-module.interesting     $archived $nagioslog
		    
rm -f /tmp/monitor-reports-module.interesting


exit 0
