#!/bin/sh

name=monitor-reports-module
mod_path=/opt/monitor/op5/reports/module
old_mod_path=/opt/monitor/op5/ndbneb
latest_db_version=2
prefix=/opt/monitor

# first fix database permissions. We do this unconditionally
mysql -e \
  'GRANT ALL ON monitor_reports.* TO monitor@localhost IDENTIFIED BY "monitor"'
mysql -e 'FLUSH PRIVILEGES'


# next we check if we need to create the database
query="SELECT id FROM report_data LIMIT 1"
mysql monitor_reports -Be "$query" >/dev/null 2>&1 || {
    echo "Creating database"
    mysqladmin create monitor_reports >/dev/null 2>&1 || :
    mysql monitor_reports < $mod_path/scripts/monitor_reports.sql
}



# since "import" supports incremental imports, we might as well
# always run it. It will never import anything if we're already
# up to date, and it will make retroactively fixing a broken module
# as simple as updating the package
php $mod_path/find_configured.php \
    > /tmp/$name.interesting


# now try to find out if there are any files. This is
# trivial to do in shell, but hard to know if it's a
# user-error or just missing files in import.c
archived="/opt/monitor/var/archives/nagios-*.log"
for f in $archived; do
    if ! [ -f "$f" ]; then
	    archived=
	fi
done
nagioslog=/opt/monitor/var/nagios.log
test -f $nagioslog || nagioslog=

query="SELECT version FROM db_version"
current_db_version=$(mysql monitor_reports -Be "$query" -N 2>/dev/null)



# if lower version than 2, run full import, set version=2
if [ $latest_db_version -eq 2 ] && [ $current_db_version -lt $latest_db_version ]; then
    echo -n "Running full database import ... ";
    test "$nagioslog$archived" && \
        $mod_path/import --truncate-db \
		$archived $nagioslog
	mysql monitor_reports -Be "UPDATE db_version SET version=$latest_db_version"
	echo "done.";
else
	echo -n "Run incremental database import ... ";
	# only run the import if there are any files
	test "$nagioslog$archived" && \
		$mod_path/import --incremental \
		  --interesting /tmp/$name.interesting \
		  $archived $nagioslog

	echo "done.";
fi


rm -f /tmp/$name.interesting

# On upgrade, make sure to point Nagios to the new module
# location, since it changed between 1.0.0 and 1.0.1
sed -i 's,$old_mod_path,$mod_path,g' $prefix/etc/nagios.cfg


# Add the module to the nagios configuration
grep -q ndbneb.so $prefix/etc/nagios.cfg || \
    sed -i 's#^log_file.*#broker_module=$mod_path/ndbneb.so $mod_path/ndbneb.conf\n\n&#' \
	    $prefix/etc/nagios.cfg

# now start monitor again
/etc/rc.d/init.d/monitor start
