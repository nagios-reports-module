#include "ndbneb.h"
#include <stdarg.h>
#include <stdio.h>
#include "sql.h"
#include "logging.h"

static struct {
	char *host;
	char *name;
	char *user;
	char *pass;
	char *table;
	unsigned int port;
	MYSQL *link;
} db;

#undef ESC_BUFS
#define ESC_BUFS 8 /* must be a power of 2 */
#define MAX_ESC_STRING 65536

typedef unsigned int uint;

#define esc(s) sql_escape(s)
char *sql_escape(const char *str)
{
	static int idx = 0;
	static char *buf_ary[ESC_BUFS] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
	static uint bufsize[ESC_BUFS] = { 0, 0, 0, 0, 0, 0, 0, 0 };
	char *buf;
	int len;

	if (!str || !*str)
		return "";

	if (!db.link && sql_init() < 0)
		return NULL;

	len = strlen(str);
	if (len > MAX_ESC_STRING) {
		lwarn("Unescaped string too large (%d bytes, %d is allowed)",
			  len, MAX_ESC_STRING);
		return "";
	}

	if (bufsize[idx] < len * 2) {
		uint size = len * 2;
		if (size < 16384)
			size = 16384;

		buf_ary[idx] = realloc(buf_ary[idx], size);
		if (!buf_ary[idx]) {
			bufsize[idx] = 0;
			lwarn("Failed to allocate %d bytes for sql escape buffer", size);
			return "";
		}
		bufsize[idx] = size;
	}
	buf = buf_ary[idx];
	idx = (idx + 1) & (ESC_BUFS - 1);
	mysql_real_escape_string(db.link, buf, str, len);

	return buf;
}

/*
 * these two functions are only here to allow callers
 * access to error reporting without having to expose
 * the db-link to theh callers. It's also nifty as we
 * want to remain database layer agnostic
 */
const char *sql_error()
{
	if (db.link)
		return mysql_error(db.link);

	return "unknown error (no link)";
}

int sql_errno(void)
{
	if (db.link)
		return mysql_errno(db.link);

	return -1;
}

SQL_RESULT *sql_get_result(void)
{
	if (db.link)
		return mysql_use_result(db.link);

	return NULL;
}

SQL_ROW sql_fetch_row(MYSQL_RES *result)
{
	if (result)
		return mysql_fetch_row(result);

	return NULL;
}

void sql_free_result(SQL_RESULT *result)
{
	if (result)
		mysql_free_result(result);
}

static int sql_reinit(void)
{
	sql_close();
	return sql_init();
}

int sql_query(const char *fmt, ...)
{
	char *query;
	int len, result = 0;
	va_list ap;

	if (!db.link && sql_reinit() < 0)
		return -1;

	va_start(ap, fmt);
	len = vasprintf(&query, fmt, ap);
	va_end(ap);

	if (len == -1 || !query) {
		lerr("sql_query: Failed to build query from format-string '%s'", fmt);
		return -1;
	}
	if ((result = mysql_real_query(db.link, query, len))) {
		lerr("mysql_query(): Retrying failed query [%s]: %s",
		     query, sql_error());

		sql_close();
		if (sql_init() < 0 || (result = mysql_real_query(db.link, query, len)))
			lerr("mysql_query(): Dropping failed query [%s]: %s",
			     query, sql_error());
	}

	free(query);

	return result;
}

int sql_init(void)
{
	if (!(db.link = mysql_init(NULL)))
		return -1;

	if (!db.host) {
		db.host = "";
		db.port = 0;
	}

	if (!(mysql_real_connect(db.link, db.host, db.user, db.pass,
							 db.name, db.port, NULL, 0)))
	{
		lerr("Failed to connect to '%s' at '%s':'%d' using %s:%s as credentials: %s",
		     db.name, db.host, db.port, db.user, db.pass, sql_error());

		sql_close();
		return -1;
	}

	return 0;
}

int sql_close(void)
{
	if (db.link)
		mysql_close(db.link);

	db.link = NULL;

	return 0;
}

const char *sql_db_name(void)
{
	if (!db.name)
		return "monitor_reports";

	return db.name;
}

const char *sql_table_name(void)
{
	if (!db.table)
		return "report_data";

	return db.table;
}

int sql_config(const char *key, const char *value)
{
	if (!prefixcmp(key, "db_database"))
		db.name = strdup(value);
	else if (!prefixcmp(key, "db_user"))
		db.user = strdup(value);
	else if (!prefixcmp(key, "db_pass"))
		db.pass = strdup(value);
	else if (!prefixcmp(key, "db_host"))
		db.host = strdup(value);
	else if (!prefixcmp(key, "db_table")) {
		db.table = strdup(value);
	}
	else if (!prefixcmp(key, "db_port")) {
		char *endp;

		db.port = (unsigned int)strtoul(value, &endp, 0);

		if (endp == value || *endp != 0)
			return -1;
	}
	else
		return -1; /* config error */

	return 0;
}
