extern int state_init(void);
extern int host_has_new_state(char *host, int state, int type);
extern int service_has_new_state(char *host, char *desc, int state, int type);
