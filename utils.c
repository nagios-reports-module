#define NSCORE
#include "nagios/objects.h"
#include "nagios/broker.h"
#include "utils.h"

/*
 * Returns the current downtime depth of the given object.
 * "type" should always either be NEBTYPE_DOWNTIME_START or
 * NEBTYPE_DOWNTIME_STOP. In case of NEBTYPE_DOWNTIME_START,
 * we know the downtime depth is at least 1, so return that.
 * This nifty little hack make these functions usable with
 * the import program, for which "find_host()" will always
 * return NULL.
 */
int dt_depth_host(int type, char *host_name)
{
	host *hst;
	int depth = (type == NEBTYPE_DOWNTIME_START);

	if ((hst = find_host(host_name)))
		depth += hst->scheduled_downtime_depth;

	return depth;
}

int dt_depth_svc(int type, char *host_name, char *service_description)
{
	service *svc;
	int depth = (type == NEBTYPE_DOWNTIME_START);

	if ((svc = find_service(host_name, service_description)))
		depth += svc->scheduled_downtime_depth;

	return depth;
}

extern host *host_list, *host_list_tail;
extern service *service_list, *service_list_tail;
extern sched_info scheduling_info;

int prime_initial_states(hash_table *host_states, hash_table *svc_states)
{
	host *h;
	service *s;
	int *arena, i;

	/*
	 * no hosts = no services = nothing to do but warn about it
	 */
	if (!host_list || !scheduling_info.total_hosts) {
		return 0;
	}

	i = 0;
	arena = calloc(scheduling_info.total_hosts, sizeof(int));
	if (!arena)
		return -1;

	for (h = host_list; h && h != host_list_tail; h = h->next) {
		int *state;
		state = &arena[i];

		*state = CAT_STATE(h->current_state, h->state_type);
		hash_add(host_states, h->name, state);
	}

	i = 0;
	arena = calloc(scheduling_info.total_services, sizeof(int));
	if (!arena)
		return -1;

	for (s = service_list; s && s != service_list_tail; s = s->next) {
		int *state;
		state = &arena[i];

		if (!state)
			return -1;

		*state = CAT_STATE(s->current_state, s->state_type);
		hash_add2(svc_states, s->host_name, s->description, state);
	}

	return 0;
}
