#ifndef UTILS_H
#define UTILS_H
#include "hash.h"

#define CAT_STATE(__state, __type) ((__state | (__type << 8)))

extern int dt_depth_host(int type, char *host_name);
extern int dt_depth_svc(int type, char *host_name, char *service_description);
extern int prime_initial_states(hash_table *host_states, hash_table *svc_states);
#endif /* UTILS_H */
