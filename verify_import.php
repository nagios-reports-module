#!/usr/bin/php
<?php

$table = 'report_data';

for ($i = 1; $i < $argc; $i++) {
	if ($argv[$i] === '--db-table') {
		$table = $argv[++$i];
	}
}

function crash($msg = false)
{
	if ($msg) {
		echo $msg . "\n";
	}

	exit(1);
}


$warnings = array();
function warn($msg)
{
	global $warnings;
#	echo $msg . "\n";

	if (!isset($warnings[$msg]))
		$warnings[$msg] = 0;

	$warnings[$msg]++;
}

function get_count($condition = false)
{
	global $table;

	$query = 'SELECT count(id) AS row_count FROM ' . $table;
	if ($condition)
		$query .= " WHERE $condition";
	$result = mysql_query($query);
	if (!$result)
		crash("mysql_query($query) failed: " . mysql_error());
	return mysql_result($result, 0, 'row_count');
}

function human_time($stamp)
{
	if (!$stamp)
		return 0;

	$days = sprintf("%.0f", $stamp / 86400);
	$hours = sprintf("%.0f", ($stamp % 86400) / 3600);
	$mins = sprintf("%.0f", ($stamp % 3600) / 60);
	$secs = sprintf("%.0f", $stamp % 60);

	$ret = "";
	if ($days)
		$ret .= $days . "d ";
	if ($hours)
		$ret .= $hours . "h ";
	if ($mins)
		$ret .= $mins . "m ";
	$ret .= $secs . "s";

	return $ret;
}

$db = mysql_connect('localhost', 'monitor', 'monitor');
if (!$db)
	crash("Failed to connect to mysql: " . mysql_errno());

mysql_select_db('monitor_reports');
$result = mysql_query('SELECT count(id) as row_count FROM ' . $table);
$tot_rows = get_count();
$stop_events = get_count('event_type = 103');
$start_events = get_count('event_type = 100');
$dt_start_events = get_count('event_type = 1103');
$dt_stop_events = get_count('event_Type = 1104');
$other_events = get_count('event_type != 100 AND event_type != 103 AND event_type != 1104 AND event_type != 1103');
echo "Total rows: $tot_rows\n";
echo "Stop events: $stop_events\n";
echo "Start events: $start_events\n";
echo "Downtime start events: $dt_start_events\n";
echo "Downtime stop events: $dt_stop_events\n";
echo "Other events: $other_events\n";

$query = 'SELECT timestamp, event_type, id, host_name, service_description ' .
	'FROM ' . $table . ' ' .
	'WHERE event_type IN (100, 103, 1103, 1104) ' .
	'ORDER BY id';
$result = mysql_query($query);
if (!$result)
	crash("mysql_query() failed: " . mysql_error());

$tot_rows = mysql_num_rows($result);
$running = false;
$rows = 0;
while ($row = mysql_fetch_array($result)) {
	$when = $row['timestamp'];
	$type = $row['event_type'];
	$id = $row['id'];
	$obj_name = $row['host_name'] . ';' . $row['service_description'];
	$rows++;

	switch ($type) {
	 case 100:
		if ($running)
			warn("Start when already running ($id)");
		$running = true;
		break;

	 case 103:
		if (!$running)
			warn("Stop when not running ($id)");

		$running = false;
		break;

	 case 1103:
		if (!isset($dt[$obj_name]))
			$dt[$obj_name] = $when;
		$dt[$obj_name]++;
		break;
	 case 1104:
		if (!isset($dt[$obj_name]))
			warn("Downtime stop without downtime start for $obj_name\n");
		else {
			$diff = $when - $dt[$obj_name];
			if ($diff > 86400)
				warn("Iffy downtime for $obj_name (".human_time($diff)."), " .
				     "started " . date("Y-m-d H:i:s", $dt[$obj_name]) .
					 " ($dt[$obj_name])");

			unset($dt[$obj_name]);
		}
		break;

	 default:
		if (!$running)
			warn("Event when not running ($id)");

		$running = true;
		break;
	}

	if (posix_isatty(STDOUT))
		printf("\rScanned $rows of $tot_rows rows (%.2f%%)",
		       ($rows / $tot_rows) * 100);
}

foreach ($dt as $obj_name => $then) {
	$diff = $when - $then;
	if ($diff > 7500)
		warn("Unfinished downtime for $obj_name (" . human_time($diff) ."), " .
			 "started " . date("Y-m-d H:i:s", $then) . " ($then)");
}
echo "\n";
$tot_warnings = 0;
foreach ($warnings as $warn => $num) {
	echo "$warn: $num\n";
	$tot_warnings += $num;
}
echo "Total rows: $rows\nTotal warnings: $tot_warnings\n";
?>
